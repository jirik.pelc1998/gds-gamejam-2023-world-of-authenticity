using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InteractionType
{ 
    None,
    Dialog,
    Animation
}

public class InteractionData
{
    public InteractionType type;
    public GameObject targetPosition;
    public GameObject targetOfLookAt;

    public ReporterEventDialogue dialog;

    
    public AnimationClip clip;

    public event System.EventHandler OnInteractionFinished;
}

public abstract class Interactive : MonoBehaviour
{
    public string VerboseName;
    public string Description;

    public bool IsInteractible { get; private set; }

    public virtual string LabelText { get => "Talk"; }

    public virtual InteractionData Interact() 
    { 
        return null; 
    }

    public virtual void SetInteractible(bool value)
    {
        IsInteractible = value;
    }
}
