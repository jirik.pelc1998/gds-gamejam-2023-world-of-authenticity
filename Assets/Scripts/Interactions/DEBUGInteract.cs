using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUGInteract : MonoBehaviour
{
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            var re = GetComponent<NPCReporter>();
            if(re != null)
            {
                if(UIManager.Instance.IsAnyUIPanelOpened)
                {
                    UIManager.Instance.ClosePanel();
                    return;
                }
                var diag = re.Interact().dialog;
                if(diag != null)
                {
                    UIManager.Instance.DisplayNPCInfo("Ms. Red Capsule", diag.message);
                }
            }
        }
    }
}
