using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class InteractiveAgent : MonoBehaviour
{
    private HashSet<Interactive> _interactiveObjects = new();
    public GameObject interactionIdentificator = null;

    public TMP_Text _interactionText;

    private Interactive _closestInteractiveObject = null;


    bool previousKey = false;
    // Update is called once per frame
    void Update()
    {
        UpdateClosest();

        var GetKeyDown = !previousKey && StarterAssetsInputs.Instance.interact;
        previousKey = StarterAssetsInputs.Instance.interact;

        //Debug.Log($"Previous {previousKey} GetKeyDown {GetKeyDown}");

        if (!GetKeyDown)
            return;
        if (UIManager.Instance.IsAnyUIPanelOpened)
        {
            UIManager.Instance.ClosePanel();
            return;
        }
        
        var data = Interact();
        if(data == null)
               return;
        switch (data.type)
        {
            case InteractionType.Dialog:
                var diag = data.dialog;
                UIManager.Instance.DisplayDialogue(_closestInteractiveObject.VerboseName,
                    diag.message);
                break;

            case InteractionType.Animation:
                break;

            case InteractionType.None:
                break;
        }
    }

    public InteractionData Interact()
    {
        if (_closestInteractiveObject == null)
            return null;
        return _closestInteractiveObject.Interact();
    }

    void UpdateClosest()
    {
        if (_interactiveObjects.Count == 0)
        {
            if (_closestInteractiveObject != null)
            {
                _closestInteractiveObject = null;
                _interactionText.gameObject.SetActive(false);
            }
            return;
        }

        Interactive closest = _interactiveObjects
            .Where(x => x.IsInteractible)
            .OrderBy(x => Vector3.Distance(transform.position, x.gameObject.transform.position))
            .FirstOrDefault();

        if (closest != null)
        {
            _interactionText.text = closest.LabelText;
            _interactionText.gameObject.SetActive(true);
        }
        else
        {
            _interactionText.gameObject.SetActive(false);
        }

        _closestInteractiveObject = closest;
        return;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null &&
            other.transform.parent.TryGetComponent(out Interactive interactive))
        {
            _interactiveObjects.Add(interactive);
            if(interactive is NPCAndroidController)
                Debug.Log("Android entered");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.parent != null &&
            other.transform.parent.TryGetComponent(out Interactive interactive))
        {
            _interactiveObjects.Remove(interactive);
        }
    }
}
