using Assets.Tools;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[Serializable]
public class ReporterEventDialogue
{
    [TextArea(3, 10)]
    public string message;
}

[CreateAssetMenu(fileName = "ReporterEvent", menuName = "ScriptableObjects/Create new ReporterEvent", order = 1)]
public class ReporterEvent : ScriptableObject
{
    public string eventName;

    public List<ReporterEventDialogue> _dialogues;

    public List<ReporterEventDialogue> _androidComments;

    public ReporterEventDialogue GetDialogue() => _dialogues.SelectRandom();
}
