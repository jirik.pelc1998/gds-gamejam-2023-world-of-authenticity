using Cinemachine;
using StarterAssets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuScript : MonoBehaviour
{
    [SerializeField] private CinemachineVirtualCamera _mainMenuVirtualCamera;
    [SerializeField] private AudioClip menuSound;
    [SerializeField] private AudioClip ingameSound;
    [SerializeField] private AudioSource source;

    private void Start()
    {
        OpenMainMenu(true);
    }

    public void OpenMainMenu(bool open)
    {
        Cursor.visible = open;
        Cursor.lockState = open ? CursorLockMode.None : CursorLockMode.Locked;
        StarterAssetsInputs.Instance.cursorInputForLook = !open;
        ThirdPersonController.Instance.IgnoreInput = open;
        _mainMenuVirtualCamera.Priority = open ? 50 : 0;
        this.gameObject.SetActive(open);
        source.clip = open ? menuSound : ingameSound;
        source.Play();
    }

    public void QuitGame()
    {
        Application.Quit();
    }
}
