using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using StarterAssets;
using UnityEngine.InputSystem;

public class Timer : MonoBehaviour
{
    public float TimeLeft;
    public bool TimerOn=false;
    public TMP_Text TimerTxt;
    public TMP_Text score_text;
    public TMP_Text final_text;
    public GameObject gameOver;
    // Start is called before the first frame update
    void Start()
    {
        TimerOn = false;
        TimerTxt.gameObject.SetActive(false);
        score_text.gameObject.SetActive(false);
    }
  

    void Update()
    {
        
        if (TimerOn)
        {
            score_text.text = "Score: " + NPCManager.Instance.score;

            if (TimeLeft > 0)
            {
                TimeLeft -= Time.deltaTime;
                updateTime(Mathf.FloorToInt(TimeLeft));
            }

            else
            {
                TimeLeft = 0;
                TimerOn = false; 
            }
        }

        if (TimeLeft < 0)
        {
            final_text.text += "\nfinal score: " + NPCManager.Instance.score;
            gameOver.SetActive(true);
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            StarterAssetsInputs.Instance.cursorInputForLook = false;
            ThirdPersonController.Instance.IgnoreInput = true;
        }
        
    }

    private void updateTime(int currentTime)
    {
        float mins = currentTime / 60;
        float sec = currentTime % 60;
        Debug.Log(mins);
        Debug.Log(sec);
        TimerTxt.text = string.Format("{0:00}: {1:00}", mins, sec);
    }

    public void begin()
    {
        TimerTxt.gameObject.SetActive(true);
        score_text.gameObject.SetActive(true);
        TimerOn=true;
    }

}
