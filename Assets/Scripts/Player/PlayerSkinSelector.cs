using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSkinSelector : MonoBehaviour
{
    [Range(0, 1)]
    public int Gender;
    [Range(0, 2)]
    public int Skin;
    [Range(0, 3)]
    public int Uniform;

    [SerializeField] private List<Renderer> models;
    [SerializeField] private List<Material> materials;

    private void OnValidate()
    {
        Render();
    }

    public void Render()
    {
        if(models.Count == 0 || materials.Count == 0)
            return;

        foreach (var model in models)
            model.gameObject.SetActive(false);
        models[Gender].gameObject.SetActive(true);

        var index = 3 * Uniform + Skin;
        models[Gender].sharedMaterial = materials[index];
    }

    public void SetGender(int value) => Gender = value;
    public void SetSkin(int value) => Skin = value;
    public void SetUniform(int value) => Uniform = value;
}
