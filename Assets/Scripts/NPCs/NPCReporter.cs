using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using UnityEngine;

[RequireComponent(typeof(NPCController))]
public class NPCReporter : Interactive
{
    public enum NPCReporterState
    {
        Error,
        Blank,
        Witness,
        Reporter,
        Pointer
    }

    public ReporterEvent _reporterEvent;
    public NPCController NPCController { get; private set; }

    [SerializeField] private GameObject _infoIcon;
    [SerializeField] private GameObject _directionIcon;
    public Vector3 _pointereTarget;
    public NPCAndroidController _witnessAndroid;

    [field: SerializeField]
    public NPCReporterState State { get; private set; } = NPCReporterState.Blank;

    private void Awake()
    {
        NPCController = GetComponent<NPCController>();
    }

    public void GiveInteraction(ReporterEvent reporterEvent)
    {
        _reporterEvent = reporterEvent;
        SetInteractible(true);
    }

    public void SetState(NPCReporterState state)
    {
        if(State == NPCReporterState.Witness && state == NPCReporterState.Pointer)
        {
            _witnessAndroid.ActivateIndicator();
        }
        State = state;
        Debug.Log("I have become " + state.ToString());

        if(State == NPCReporterState.Blank)
        {
            NPCController.SetBehaviour(NPCManager.Instance.BuildStupidNPCBehaviour(NPCController));
        }
        else
        {
            NPCController.SetBehaviour(NPCManager.Instance.BuildWhitnes(this, _witnessAndroid));
        }

        switch (State)
        {
            case NPCReporterState.Blank:
                _witnessAndroid = null;
                _reporterEvent = null;
                _infoIcon.SetActive(false);
                _directionIcon.SetActive(false);
                SetInteractible(false);
                break;
            case NPCReporterState.Witness:
            case NPCReporterState.Reporter:
                SetInfoIcon();
                SetInteractible(true);
                break;
            case NPCReporterState.Pointer:
                SetPointer(_pointereTarget);
                _infoIcon.SetActive(false);
                SetInteractible(false);
                break;
        }
    }

    public override InteractionData Interact()
    {
        if(State == NPCReporterState.Witness)
        {
            SetState(NPCReporterState.Pointer);
        }

        return new InteractionData
        {
            type = InteractionType.Dialog,
            dialog = _reporterEvent.GetDialogue(),
        };
    }

    public void SetInfoIcon()
    {
        _infoIcon.SetActive(true);
        if (DOTween.TotalTweensById(_infoIcon.transform) == 0)
        {
            _infoIcon.transform.DOLocalMoveY(_infoIcon.transform.localPosition.y + 0.5f, 0.5f)
                .SetLoops(-1, LoopType.Yoyo)
                .SetId(_infoIcon.transform);
            _infoIcon.transform.DOLocalRotate(Vector3.zero, 3f, RotateMode.LocalAxisAdd)
                .SetRelative()
                .SetEase(Ease.Linear)
                .SetLoops(-1, LoopType.Yoyo)
                .SetId(_infoIcon.transform);
        }

    }

    public void SetPointer(Vector3 worldtarget)
    {
        _directionIcon.SetActive(true);
        _directionIcon.transform.LookAt(worldtarget, Vector3.up);
    }
}
