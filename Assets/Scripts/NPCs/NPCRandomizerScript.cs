using Assets.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCRandomizerScript : MonoBehaviour
{
    [SerializeField] private List<GameObject> models;

    [SerializeField] private List<Material> materials;  


    // Start is called before the first frame update
    void Start()
    {
        foreach (var model in models)
            model.SetActive(false);

        var active = models.GetRandom();
        active.SetActive(true);


        var material = materials.GetRandom();
        active.GetComponent<Renderer>().sharedMaterial = material;
    }
}
