using MyBox;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(Rigidbody))]
[RequireComponent(typeof(NavMeshAgent))]
public class NPCController : MonoBehaviour
{
    public enum NPCState
    {
        Error,
        Iddle,
        Walking,
    }


    public float ArivalDistance;

    [SerializeField] private NPCState _state;
    public NPCState State => _state;

    public event System.EventHandler Arrived;

    public Rigidbody Rigidbody => _rigidbody;
    public NavMeshAgent NavMeshAgent => _navMeshAgent;
    public Animator Animator => _animator;

    public bool CreateAnimationOverrideOnAwake = false;

    private Rigidbody _rigidbody;
    private NavMeshAgent _navMeshAgent;
    private Animator _animator;
    private AnimatorOverrideController _animatorRuntimeOverrideController;
    private Vector3 _destination;
    private bool _HasUniqueAnimator = false;

    private IEnumerator _activeBehaviour;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _navMeshAgent = GetComponent<NavMeshAgent>();
        _animator = GetComponentInChildren<Animator>();
        _state = NPCState.Iddle;
        if (CreateAnimationOverrideOnAwake)
            CreateAnimationOverride();
    }

    private void Update()
    {
        if (_animator)
        {
            _animator.SetFloat("Speed", _navMeshAgent.velocity.magnitude);
            _animator.SetFloat("MotionSpeed", 1);
        }

        if (_state == NPCState.Walking && Vector3.Distance(transform.position, _destination) < ArivalDistance)
        {
            _state = NPCState.Iddle;
            Arrived?.Invoke(this, new System.EventArgs());
        }
    }

    public void GoTo(Vector3 target)
    {
        _navMeshAgent.destination = target;
        _destination = target;
        _state = NPCState.Walking;
    }

    public void Stop()
    {
        _navMeshAgent.isStopped = true;
    }

    public void SetAnimationOverride(AnimatorOverrideController animatorOverrideController)
    {
        _animator.runtimeAnimatorController = animatorOverrideController;
    }

    public void CreateAnimationOverride()
    {
        _animatorRuntimeOverrideController = new AnimatorOverrideController(_animator.runtimeAnimatorController);
        _animator.runtimeAnimatorController = _animatorRuntimeOverrideController;
        _HasUniqueAnimator = true;
    }

    public void Animate(string key, AnimationClip clip)
    {
        if (!_HasUniqueAnimator)
            CreateAnimationOverride();

        _animatorRuntimeOverrideController[key] = clip;
    }

    public void SetAction(bool value)
    {
        _animator.SetBool("Action", value);
    }

    public void SetWave(bool wave)
    {
        _animator.SetBool("Wave", wave);
    }

    public void SetBehaviour(IEnumerator behaviour)
    {
        if (_activeBehaviour != null)
            StopCoroutine(_activeBehaviour);

        _activeBehaviour = behaviour;
        StartCoroutine(__CoroutineWrapper(_activeBehaviour));


        IEnumerator __CoroutineWrapper(IEnumerator run)
        {
            yield return run;
            _activeBehaviour = null;
        }

    }
}
