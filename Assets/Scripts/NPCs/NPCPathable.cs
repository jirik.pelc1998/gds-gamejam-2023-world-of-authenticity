using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCPathable : MonoBehaviour
{
    public Vector3 CenterOffset = new Vector3(-2.5f, 0, -2.5f);

    public Vector3 Center;

    private void Start()
    {
        var obj = new GameObject("target");
        obj.transform.SetParent(transform, false);
        obj.transform.localPosition = CenterOffset;
        obj.transform.localRotation = Quaternion.identity;
        Center = obj.transform.position;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.black;
        Gizmos.DrawSphere(transform.position + transform.rotation * CenterOffset, 0.5f);
    }
}
