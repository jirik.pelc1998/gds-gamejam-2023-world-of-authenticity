using DG.Tweening;
using MyBox;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(NPCController))]
public class NPCAndroidController : Interactive
{

    private NPCController _NPCController;
    [SerializeField] private ReporterEvent _reporterEvent;
    private AndriodConfig _config;

    [SerializeField] private int _DialogProgression;

    public NPCController NPCController => _NPCController;

    public AnimationClip AndroidKillAnimation;

    public GameObject _indicatorGO;

    private bool _interacted = false;

    public override string LabelText
    {
        get => _interacted ? "Arrest" : "Talk";
    }

    #region reporters

    HashSet<NPCReporter> _NPCsInArea = new HashSet<NPCReporter>();

    HashSet<NPCReporter> _activeWitnesses = new HashSet<NPCReporter>();

    HashSet<NPCReporter> _myReporters = new HashSet<NPCReporter>();

    void PromoteNPCToWitness()
    {
        //try to find old report first
        
        var oldReportersInArea = _myReporters.Except(_activeWitnesses);
        if(oldReportersInArea.Any())
        {
            var oldReporter = oldReportersInArea.GetRandom();
            _activeWitnesses.Add(oldReporter);
            oldReporter.SetState(NPCReporter.NPCReporterState.Witness);
            return;
        }


        var npcs = _NPCsInArea.Except(_myReporters).Where(r => r.State == NPCReporter.NPCReporterState.Blank);
        if (!npcs.Any())
            return;
        var npcReporter = npcs.GetRandom();
        _activeWitnesses.Add(npcReporter);
        _myReporters.Add(npcReporter);
        npcReporter._reporterEvent = _reporterEvent;
        npcReporter._witnessAndroid = this;
        npcReporter.SetState(NPCReporter.NPCReporterState.Witness);
    }

    public void ActivateIndicator()
    {
        _indicatorGO.SetActive(true);
        if (DOTween.TotalTweensById(_indicatorGO.transform) != 0)
            return;

        _indicatorGO.transform
            .DOScale(2.5f, 1.1f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetId(_indicatorGO.transform);

        _indicatorGO.transform
            .DORotate(new Vector3(0, 360, 0), 1.5f, RotateMode.FastBeyond360)
            .SetLoops(-1, LoopType.Incremental)
            .SetId(_indicatorGO.transform);

        _indicatorGO.transform
            .DOLocalMoveY(_indicatorGO.transform.localPosition.y + 0.5f, 1f)
            .SetLoops(-1, LoopType.Yoyo)
            .SetEase(Ease.InOutSine)
            .SetId(_indicatorGO.transform);
    }

    #endregion

    private void Awake()
    {
        _NPCController = GetComponent<NPCController>();
        NPCManager.Instance._activeAndroids.Add(this);
        SetInteractible(true);
        _indicatorGO.SetActive(false);
    }

    private void Update()
    {
        if(!IsInteractible)
            return;
        if (_activeWitnesses.Count < NPCManager.Instance.ReportersPerAndroid)
            PromoteNPCToWitness();
    }

    public void SetConfig(AndriodConfig config, bool _static)
    {
        _config = config;
        _reporterEvent = config.dialog;

        if (_static)
        {
            NPCController.Animate("Action", config.behaviour);
            NPCController.SetAction(true);
        }
        else
        {
            NPCController.Animate("Walk_N", config.behaviour);
            NPCController.Animate("Run_N", config.behaviour);
        }

    }

    public override InteractionData Interact()
    {
        NPCController.SetBehaviour(NPCManager.Instance.TalkToPolice(this));
        if(!_interacted)
        {
            _interacted = true;
            return new InteractionData
            {
                type = InteractionType.Dialog,
                dialog = _reporterEvent._androidComments.GetRandom(),
            };
        }
        else
        {
            Identify();

            return new InteractionData
            {
                type = InteractionType.None,
            };
        }
    }

    [ButtonMethod]
    public void Identify()
    {
        Debug.Log("Is caught");

        _NPCController.NavMeshAgent.enabled = false;
        _NPCController.Animator.enabled = true;
        _NPCController.Animate("Action", AndroidKillAnimation);
        _NPCController.SetAction(true);
        NPCManager.Instance.score++;
        NPCManager.Instance._activeAndroids.Remove(this);
        SetInteractible(false);

        foreach (var npc in _activeWitnesses)
        {
            npc.SetState(NPCReporter.NPCReporterState.Blank);
        }
        StartCoroutine(DeathAnimation());

    }

    private IEnumerator DeathAnimation()
    {
        yield return new WaitForSeconds(AndroidKillAnimation.length+0.5f);
        transform
            .DOMoveY(transform.position.y + 100, 8f)
            .SetEase(Ease.InExpo)
            .OnComplete(() => Destroy(gameObject));

        transform
            .DORotate(new Vector3(0, 360, 0), 8f, RotateMode.FastBeyond360)
            .SetEase(Ease.InExpo);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.transform.parent != null && other.transform.parent.TryGetComponent(out NPCReporter npc))
        {
            _NPCsInArea.Add(npc);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.transform.parent != null && other.transform.parent.TryGetComponent(out NPCReporter npc))
        {
            _NPCsInArea.Remove(npc);
            
            if(_activeWitnesses.Contains(npc))
            {
                _activeWitnesses.Remove(npc);
                if (npc.State == NPCReporter.NPCReporterState.Witness)
                {
                    npc._pointereTarget = transform.position;
                    npc.SetState(NPCReporter.NPCReporterState.Reporter);
                }
            }
        }
    }
}
