using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class SimpleFollowScript : MonoBehaviour
{
    private Camera mainCamera;
    private NavMeshAgent agent;

    private void Awake()
    {
        mainCamera = Camera.main; // Assuming the script is attached to a GameObject with a camera.
        agent = GetComponent<NavMeshAgent>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) // 0 represents the left mouse button
        {
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                // Do something with the hit point
                Debug.Log("Hit " + hit.point);
                agent.SetDestination(hit.point);
                // You can also access hit.collider to get the object that was hit.
            }
        }
    }
}
