using Assets.Tools;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class FootStepScript : MonoBehaviour
{
    public List<AudioClip> clipVariants;

    private AudioSource _audioSource;


    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }


    // Start is called before the first frame update
    void OnFootstep()
    {
        if (clipVariants.Count == 0)
            return;

        _audioSource.clip = clipVariants.SelectRandom();
        _audioSource.Play();
    }
}
