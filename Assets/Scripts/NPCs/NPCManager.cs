using Assets.Tools;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;


[System.Serializable]
public class AndriodConfig
{
    public string Action;
    public AnimationClip behaviour;
    public ReporterEvent dialog;
    public Color color;
}
public class NPCManager : Singleton<NPCManager>
{



    private List<NPCPathable> _pathables;
    [SerializeField] private List<NPCPathable> _spawnPoints;
    [SerializeField] private NPCController _NPCPrefab;
    [SerializeField] private NPCAndroidController _NPCAndroidPrefab;
    [SerializeField] private int _NPCCount;
    [field: SerializeField] public int ReportersPerAndroid { get; private set; }
    [field: SerializeField] public int AndroidsMax { get; private set; }

    public HashSet<NPCAndroidController> _activeAndroids = new();

    public GameObject Player;

    [SerializeField]
    private float minDistanceFromPlayer = 50f;

    public int score = 0;

    [Header("Name assets")]
    public TextAsset femaleFirstNameAsset;
    public TextAsset maleFirstNameAsset;
    public TextAsset lastNameAsset;

    public List<string> _femaleFirstNames;
    public List<string> _maleFirstName;
    public List<string> _lastName;

    [Header("clips")]
    public AnimationClip TPose;
    public AnimationClip WaveForHelp;
    public AnimationClip Cry;
    public AnimationClip CapturedPose;

    [Header("Androids")]
    public List<AndriodConfig> StaticPose;
    public List<AndriodConfig> MovingVierd;



    protected override void Awake()
    {
        base.Awake();
        _pathables = GetComponentsInChildren<NPCPathable>().ToList();

        _femaleFirstNames = femaleFirstNameAsset.text
            .Split(System.Environment.NewLine, System.StringSplitOptions.RemoveEmptyEntries)
            .ToList();
        _maleFirstName = maleFirstNameAsset.text
            .Split(System.Environment.NewLine, System.StringSplitOptions.RemoveEmptyEntries)
            .ToList();
        _lastName = lastNameAsset.text
            .Split(System.Environment.NewLine, System.StringSplitOptions.RemoveEmptyEntries)
            .ToList();
    }

    // Start is called before the first frame update
    void Start()
    {
        _spawnPoints = _pathables.SelectRandom(_NPCCount).ToList();

        foreach (var pathable in _spawnPoints)
        {
            var npc = InstatiateNPC(_NPCPrefab, pathable.transform.position);
            npc.SetBehaviour(BuildStupidNPCBehaviour(npc)); 
            npc.GetComponent<NPCReporter>().VerboseName = _femaleFirstNames.SelectRandom() + " " + _lastName.SelectRandom();
        }

        StartCoroutine(AndroidSpawner());
    }


    void SpawnAndroid()
    {
        var pathable = _spawnPoints
            .Where(sp => Vector3.Distance(Player.transform.position,sp.Center) > minDistanceFromPlayer/2) //TODO < -> >
            .ToList()
            .SelectRandom();
        var android = InstatiateNPC(_NPCAndroidPrefab, pathable.transform.position);
        
        bool _static = Random.value > 0.5f;
        var aconf = _static ? StaticPose.SelectRandom() : MovingVierd.SelectRandom();
        android.SetConfig(aconf,_static);

        android.VerboseName = _femaleFirstNames.SelectRandom() + " " + _lastName.SelectRandom();

        if (_static)
            android.NPCController.SetBehaviour(StaticBehaviour(android));
        else
            android.NPCController.SetBehaviour(BuildStupidNPCBehaviour(android.NPCController));
    }

    public IEnumerator BuildStupidNPCBehaviour(NPCController npc)
    {
        while (true)
        {
            npc.GoTo(_pathables.SelectRandom().transform.position);
            yield return new WaitUntil(() => npc.State == NPCController.NPCState.Iddle);
        }
    }

    public IEnumerator BuildAndroidTPose(NPCAndroidController android)
    {
        var npc = android.NPCController;
        npc.Animate("Action", TPose);
        npc.SetAction(true);
        yield return BuildStupidNPCBehaviour(npc);
    }

    public IEnumerator BuildWhitnes(NPCReporter reporter, NPCAndroidController andriod)
    {
        var npc = reporter.NPCController;
        npc.Stop();

        while (reporter.State == NPCReporter.NPCReporterState.Witness)
        {
            //npc.transform.LookAt(andriod.transform.position, Vector3.up);
            yield return null;
        }
    }

    public IEnumerator StaticBehaviour(NPCAndroidController andriod)
    {
        var npc = andriod.NPCController;
        npc.Stop();
        yield return null;
    }

    public IEnumerator TalkToPolice(NPCAndroidController andriod)
    {
        var npc = andriod.NPCController;
        while(true)
        {
            npc.transform.LookAt(Player.transform.position, Vector3.up);
            yield return null;
        }
    }

    private IEnumerator AndroidSpawner()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.1f);
            if (_activeAndroids.Count < AndroidsMax)
                SpawnAndroid();
        }
    }

    private TType InstatiateNPC<TType>(TType script, Vector3 position)
        where TType : MonoBehaviour
    {
        var result = Instantiate<TType>(script, position, Quaternion.identity, transform);
        var npc = result.GetComponent<NPCController>();
        npc.name = $"{_maleFirstName.SelectRandom()} {_lastName.SelectRandom()}"; //TODO: reflect gender
        npc.NavMeshAgent.speed = Random.Range(2f, 3f);
        return result;
    }
}
