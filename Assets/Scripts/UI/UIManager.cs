using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIManager : Singleton<UIManager>
{
    [SerializeField]
    private TMP_Text _dialogueHeading, _dialogueText, _infoHeading, _infoText;
    [SerializeField]
    private GameObject _dialoguePanel, _infoPanel, _policeRadioAlertIcon;

    private string _policeRadioAlertHeading, _policeRadioAlertText;

    private GameObject _currentUIPanelOpened;

    public bool IsAnyUIPanelOpened => _currentUIPanelOpened != null;

    public void ClosePanel()
    {
        if(_currentUIPanelOpened != null)
        {
            _currentUIPanelOpened.SetActive(false);
            _currentUIPanelOpened = null;
        }
    }

    public void DisplayNPCInfo(string heading, string text)
    {
        _infoHeading.text = heading.ToLower();
        _infoText.text = text;

        _currentUIPanelOpened = _infoPanel;
        _infoPanel.SetActive(true);
    }

    public void DisplayDialogue(string heading, string text)
    {
        _dialogueHeading.text = heading.ToLower();
        _dialogueText.text = text;

        _currentUIPanelOpened = _dialoguePanel;
        _dialoguePanel.SetActive(true);
    }   


    public void ReceivePoliceRadioAlert(string heading, string text)
    {
        _policeRadioAlertHeading = heading;
        _policeRadioAlertText = text;
        _policeRadioAlertIcon.SetActive(true);

    }

    public void DisplayPoliceRadioDialogue()
    {
        if(_policeRadioAlertIcon.activeSelf)
        {
            DisplayDialogue(_policeRadioAlertHeading, _policeRadioAlertText);
            _policeRadioAlertIcon.SetActive(false);
        }
        else
        {
            Debug.LogWarning("No police radio alert to display");
        }
        
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

}
