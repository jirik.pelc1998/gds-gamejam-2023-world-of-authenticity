using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.AI.Navigation;
using UnityEditor;
using UnityEngine;

public class DevScripts
{
    [MenuItem("Assets/DevScripts/Test()")]
    public static void Test()
    {
        var data = GetSelected<GameObject>();
        foreach (var item in data)
        {
            Debug.Log(item.name);
        }
    }

    [MenuItem("Assets/DevScripts/SetStatic(true)")]
    public static void SetStaticTrue() => SetStatic(true);

    [MenuItem("Assets/DevScripts/SetStatic(false)")]
    public static void SetStaticFalse() => SetStatic(false);

    [MenuItem("Assets/DevScripts/SetNavMeshSurface()")]
    public static void SetNavMeshSurface()
    {
        foreach (var prefab in GetSelected<GameObject>())
        {
            var surface = prefab.AddComponent<NavMeshSurface>();
            EditorUtility.SetDirty(prefab);
            AssetDatabase.SaveAssetIfDirty(prefab);
        }
    }

    [MenuItem("Assets/DevScripts/AddNPCPathable()")]
    public static void AddNPCPathable()
    {
        foreach (var prefab in GetSelected<GameObject>())
        {
            var pathable = prefab.AddComponent<NPCPathable>();
            EditorUtility.SetDirty(prefab);
            AssetDatabase.SaveAssetIfDirty(prefab);
        }
    }

    private static void SetStatic(bool @static)
    {
        foreach (var prefab in GetSelected<GameObject>())
        {
            prefab.isStatic = @static;
            EditorUtility.SetDirty(prefab);
            AssetDatabase.SaveAssetIfDirty(prefab);
        }
    }

    private static IEnumerable<T> GetSelected<T>() where T : UnityEngine.Object
    {
        return Selection.assetGUIDs
            .Select(x => AssetDatabase.GUIDToAssetPath(x))
            .Select(x => AssetDatabase.LoadAssetAtPath<T>(x))
            .Where(x => x != null);
    }

}
